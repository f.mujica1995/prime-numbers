FROM node:14.16.0

ENV NODE_ENV=production
ENV BASE=api
ENV VERSION=v1
ENV PORT=3000

WORKDIR /app

COPY ["package.json", "package-log.json*", "./"]

RUN npm install --production

COPY . .

CMD ["npm", "run", "dev"]


  