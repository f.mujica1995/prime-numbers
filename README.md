# Api para obtener números primos dado un número

## Configuración Local

## Ejecución Local

Para correr la api con nodemon se debe ejecutar el siguiente comando: 
```bash
npm run dev
```
Para correr la api una vez ejecutar el siguiente comando: 
```bash
node src/app
```

## Dockerizar app

Para dockerizar la app deberá ejecutar el archivo Dockerfile con el siguiente comando (damos como nombre de tag el nombre del proyecto): 
```bash
sudo docker build -t prime-numbers .
```
Para ejecutar el container por primera vez (asignamos un nombre, en ese caso seguimos con el nombre del proyecto)
```bash
sudo docker run -it -p 3000:3000 --name prime-numbers prime-numbers
```
Para volver a ejecutar luego de haber asignado un nombre
```bash
sudo docker run -it -p 3000:3000 prime-numbers
```
## Ejecución de tests

Para correr los tests se debe ejecutar el siguiente comando: 
```bash
npm run test
```
## Variables de Entorno 

Las siguientes variables de entorno se encuentran definidas en el archivo environment.js

### generales

* PUERTO=3000
* BASE=api
* VERSION=v1

## Operaciones
| **Verbo HTTP** | **Endpoint** | **Operación** |
| ------ | ------ | ------ |
| GET | /api/v1/prime-numbers/:number | Obtener números primos. |

#### URL de ejemplo enviando el número 15:
```bash
    http://localhost:3000/api/v1/prime-numbers/15
```