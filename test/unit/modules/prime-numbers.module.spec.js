const { getPrimeNumbersModule } = require('../../../src/app/modules/prime-numbers.module'); 
const { createRequest, createResponse } = require('node-mocks-http');

beforeEach(() => {
    request = createRequest();
    response = createResponse();
});

describe('Test for prime numbers module', () => {
    it('should get a good response', async () => {
        const result = await getPrimeNumbersModule({ params: { number: 15 }}, response);
        expect(result.code).toBe(200);
    });

    it('should get a data validation', async () => {
        const result = await getPrimeNumbersModule({ params: { number: "INCORRECTO" }}, response);
        expect(result.code).toBe(400);
    });

    it('should get a data validation for a number less than 2', async () => {
        const result = await getPrimeNumbersModule({ params: { number: 0 }}, response);
        expect(result.code).toBe(400);
    });
});