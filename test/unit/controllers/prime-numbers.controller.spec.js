const {
    getPrimeNumbersModule,
} = require("../../../src/app/modules/prime-numbers.module");
const {
    getPrimeNumbersController,
} = require("../../../src/app/controllers/prime-numbers.controller");
const { createRequest, createResponse } = require("node-mocks-http");

beforeEach(() => {
    request = createRequest();
    response = createResponse();
});

describe("Test for prime numbers module", () => {
    it("should get a good response", async () => {
        getPrimeNumbersModule.prototype = jest.fn().mockResolvedValue();
        await getPrimeNumbersController({ params: { number: 15 } }, response);
        expect(response.statusCode).toBe(200);
    });
});
