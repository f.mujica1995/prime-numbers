const request = require('supertest');
const server = require('../../src/server/server').server; 

describe('Testing for routes', () => {
    it('should return a good response', async () => {
        request(server).get('/api/v1/prime-numbers', (error, response) => {
            expect(response.code).toBe(200);
        }).send(15);
    });
});