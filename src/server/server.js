const express = require("express");
const bodyParser = require("body-parser");
const listEndpoints = require("express-list-endpoints");
const routes = require('../routes/index').appRoutes;

const server = express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(routes);

const endpoints = listEndpoints(routes);

module.exports = {
    server,
    endpoints
};
