const dotenv = require('dotenv');

dotenv.config();

const APP = {
    PORT: process.env.PORT,
    BASE: process.env.BASE,
    VERSION: process.env.VERSION
};

module.exports = {
    APP
};