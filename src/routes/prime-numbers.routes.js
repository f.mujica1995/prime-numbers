const { Router } = require('express');
const { getPrimeNumbersController } = require('../app/controllers/prime-numbers.controller');

const primeNumberRoutes = Router();

primeNumberRoutes.get('/prime-numbers/:number', getPrimeNumbersController);

module.exports = {
    primeNumberRoutes
}