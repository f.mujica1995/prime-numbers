const { primeNumberRoutes } = require('./prime-numbers.routes');
const environment = require('../environment/environment').APP;

const express = require('express');

const appRoutes = express();

const base = `/${environment.BASE}/${environment.VERSION}`;

appRoutes.use(base, primeNumberRoutes);

module.exports = {
    appRoutes
}