const { server, endpoints } = require("./server/server");
const isPrime = require("./app/utils/prime-numbers.util").isPrime;
const environment = require("./environment/environment").APP;
const numberSchema = require("./app/schemas/prime-numbers.schema").numberSchema;

function main() {
    server.listen(environment.PORT || 3000);
    console.log(`Servidor corriendo en el puerto ${environment.PORT}`);
    console.log("Rutas: ", endpoints);

    let numbers = [];

    process.stdout.write("Escriba un número: ");

    process.stdin.on("data", (data) => {
        // Por defecto la consola nos devuelve tipo 'object', así que hacemos el parseo a número y validamos 
        let number = parseInt(data);
        let { error, value } = numberSchema.validate(number);

        if (error) {
            console.log("Error, debes ingresar un valor númerico");
        } else {
            numbers = isPrime(value);
            process.stdout.write(`Los números primos menores o igual a ${value} son ${numbers}`);
            process.exit();
        }
    });
}

main();
