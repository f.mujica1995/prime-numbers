const { getPrimeNumbersModule } = require("../modules/prime-numbers.module");

async function getPrimeNumbersController(req, res) {
    const result = await getPrimeNumbersModule(req, res);
    res.status(result.code).send(result);
}

module.exports = {
    getPrimeNumbersController
};
