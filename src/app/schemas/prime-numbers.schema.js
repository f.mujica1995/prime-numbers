const Joi = require('joi');

const numberSchema = Joi.number().required();

module.exports = {
    numberSchema
}