const isPrime = (num) => {
    arr = [];
    for (let i = 2; i <= num; i++) {
        let flag = 0;

        for (let j = 2; j < i; j++) {
            if (i % j == 0) {
                flag = 1;
                break;
            }
        }

        if (i > 1 && flag == 0) {
            arr.push(i);
        }
    }
    
    // Retornamos el arrelgo de manera decendente
    return arr.sort((a, b) =>  b - a);
};

module.exports = {
    isPrime
}
