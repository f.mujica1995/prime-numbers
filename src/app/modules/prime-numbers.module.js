const isPrime = require("../utils/prime-numbers.util").isPrime;
const numberSchema = require("../schemas/prime-numbers.schema").numberSchema;


/**
 * @param {number} req
 */
async function getPrimeNumbersModule(req) {
    let { error, value } = numberSchema.validate(req.params.number);
    if (value < 2) {
        return {
            code: 400,
            message: "Debe ingresar un número mayor o igual a 2",
            payload: {},
        };
    }
    if (error) {
        return {
            code: 400,
            message: "Parámetro incorrecto, por favor ingrese un número",
            payload: error,
        };
    } else {
        const primeNumbers = isPrime(value);
        return { code: 200, message: "OK", payload: primeNumbers };
    }
}

module.exports = {
    getPrimeNumbersModule,
};
